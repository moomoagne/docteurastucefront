package sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.model;

public class Conseil {
    private Integer idConseil;

    private String titre;

    private String contenu;
    String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIdConseil() {
        return idConseil;
    }

    public void setIdConseil(Integer idConseil) {
        this.idConseil = idConseil;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;

    }

    public Conseil() {

    }

    public Conseil(String titre, String contenu) {
        this.titre = titre;
        this.contenu = contenu;
    }
}
