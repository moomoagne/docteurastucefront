package sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.model;

public class Value {
    String value;
    String message;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
