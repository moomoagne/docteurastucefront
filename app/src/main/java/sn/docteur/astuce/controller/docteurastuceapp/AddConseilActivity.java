package sn.docteur.astuce.controller.docteurastuceapp;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import sn.docteur.astuce.controller.docteurastuceapp.server.CustomRequest;
import sn.docteur.astuce.controller.docteurastuceapp.server.ServeurAcces;


public class AddConseilActivity extends AppCompatActivity {

    @BindView(R.id.input_contenu) EditText _contenuText;
    @BindView(R.id.input_titre) EditText _titreText;
    @BindView(R.id.btn_conseil) Button _conseilButton;
    TextView _signupLink;
    private static final String TAG = "ConseilActivity";
    public static final String URL = ServeurAcces.url ;
    ArrayList<HashMap<String, String>> Item_List;
    private ProgressDialog progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_conseil);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null){
            getSupportActionBar().setTitle("Insertion conseil");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        _conseilButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                conseil();
            }
        });


    }

    public void conseil() {
        Log.d(TAG, "conseil");

        if (!validate()) {
            onConseilFailed();
            return;
        }

        _conseilButton.setEnabled(false);

        //final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
        //      R.style.AppTheme_Dark_Dialog);
        //progressDialog.setIndeterminate(true);
        //  progressDialog.setMessage("Authenticating...");
        // progressDialog.show();

        String contenu = _contenuText.getText().toString();
        String titre = _titreText.getText().toString();

        // TODO: Implement your own authentication logic here.

        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onConseilSuccess();

                    }
                }, 3000);
    }




    @Override
    public void onBackPressed() {
        // disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onConseilSuccess() {
        _conseilButton.setEnabled(true);
        finish();
    }

    public void onConseilFailed() {
        Toast.makeText(getBaseContext(), "Conseil failed", Toast.LENGTH_LONG).show();

        _conseilButton.setEnabled(true);
    }
    public boolean validate() {
        boolean valid = true;
        //membuat progress dialog
        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading ...");
       // progress.show();
        String contenu = _contenuText.getText().toString();
        String titre = _titreText.getText().toString();

        if (contenu.isEmpty() ) {
            _contenuText.setError("Le contenu ne doit pas etre vide");
            valid = false;
        } else {
            _contenuText.setError(null);
        }

        if (titre.isEmpty() ) {
            _titreText.setError("Le titre ne doit pas etre vide");
            valid = false;
        } else {
            _titreText.setError(null);
        }
        SignUpUser();

        return valid;
    }
String message;
    private void SignUpUser() {
        progress.show();
        // Optional Parameters to pass as POST request
        JSONObject js = new JSONObject();
        try {
            js.put("titre",_titreText.getText().toString());
            js.put("contenu",_contenuText.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Make request for JSONObject
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, URL+"conseils/"+1+"", js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            int success = response.getInt("status");

                            if (success == 0) {


                                progress.dismiss();
                                AddConseilActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        //Intent i = new Intent(getApplicationContext(), MainNotifActivity.class);
                                        Intent i = new Intent(getApplicationContext(), AccueilActivity.class);
                                        startActivity(i);

                                        // closing this screen
                                        finish();
                                    }
                                });

                            } else {
                                message = response.getString("message");
                                progress.dismiss();

                                //Log.v("Message connexion", message);
                                AddConseilActivity.this.runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, response.toString() + " i am queen");
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
            }
        }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        // Adding request to request queue
        Volley.newRequestQueue(this).add(jsonObjReq);

    }
}
