package sn.docteur.astuce.controller.docteurastuceapp.sn.docteur.astuce.controller.docteurastuceapp.model;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    private int id;
    private String firstName, lastName, telephone, activationToken, email, password;
    private boolean isActive;

    private String profilePicture;

    public User() {}

    public User(int id) { this.id = id; }

    public User(String telephone) {
        this.telephone = telephone;
    }

    public User(int id, String telephone) {
        this.id = id;
        this.telephone = telephone;
    }

    public User(int id, String firstName, String lastName, String telephone, String activationToken) {
        this(id, telephone);
        this.firstName = firstName;
        this.lastName = lastName;
        this.activationToken = activationToken;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getActivationToken() {
        return activationToken;
    }

    public void setActivationToken(String activationToken) {
        this.activationToken = activationToken;
    }

    public void setActive(boolean active) {
        this.isActive = active;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public String getPassword() {
        return password;
    }
}
